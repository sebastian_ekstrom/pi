Som du säkert vet är kvoten mellan en cirkels omkrets och dess diameter
lika med pi. Detta visste också den grekiske matematikern Arkimedes
redan på 200-talet före Kristus, något som han använde för att ta fram
en metod för att uppskatta värdet på pi som skulle komma att användas
i över 1000 år.

Metoden bygger på att man kan uppskatta en cirkel med en regelbunden
månghörning så stor att den precis får plats inuti cirkeln; ju fler
kanter månghörningen har, desto närmare cirkeln kommer den, och desto
bättre blir uppskattningen. Till skillnad från cirkeln består
månghörningen endast av räta linjer, vilkas längd är lätt att beräkna
med Pythagoras sats, Därmed kan man få ett exakt värde på månghörningens
omkrets, och eftersom detta värde är ungefär like med cirkelns omkrets
kan man använda det för att få ut ett ungefärligt värde på pi.

(bild på Arkimedes metod: fyra, åtta, sexton hörn)

Eftersom alla sidor i månghörningen är like långa räcker det att man
räknar ut längden på en av dem, eftersom man då kan få ut hela omkretsen
genom att multiplicera med antalet sidor. Detta innebär att man bara
behöver hitta koordinaterna för en ny punkt i varje steg av metoden.

(bild på konstruktion av 4 -> 8 hörn)

Implementera Arkimedes metod för att beräkna pi.
