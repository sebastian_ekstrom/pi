import math

def dist(x1, y1, x2, y2):
    return math.sqrt((x1-x2)**2 + (y1-y2)**2)
    
def next_point(x, y):
    d = dist((x+1)/2, y/2, 0, 0)
    x_new = d
    y_new = math.sqrt(1 - d**2)
    return x_new, y_new
    
x = 0
y = 1
sides = 4

for i in range(10):
    x, y = next_point(x, y)
    sides *= 2
    pi = dist(x, y, 1, 0) * sides/2
    print(pi)
